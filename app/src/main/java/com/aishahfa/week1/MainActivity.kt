package com.aishahfa.week1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener {
            doAssignment1()
        }

        button2.setOnClickListener {
            doAssignment2()
        }
    }

    private fun doAssignment1(){
        startActivity(Intent(this, Week1_Assignment1::class.java))
    }

    private fun doAssignment2(){
        startActivity(Intent(this, Week1_Assignment2::class.java))
    }
}
